(function ($) {

	$.FroalaEditor.DEFAULTS = $.extend($.FroalaEditor.DEFAULTS, {
		imageCaptionButtons: ['imageBack', '|']
	});

	$.extend($.FroalaEditor.POPUP_TEMPLATES, {
		'image.caption': '[_BUTTONS_][_CAPTION_LAYER_]'
	});

	// Define the plugin.
	// The editor parameter is the current instance.
	$.FroalaEditor.PLUGINS.imageCaption = function (editor) {

		var $current_image;

		function _init () {
			editor.events.on('image.beforeRemove', function() {
				var $current_image = editor.image.get();
				if ($current_image.parent().parent().hasClass('post-caption-container')) {
					$current_image.parent().parent().replaceWith('<p><br></p>');
				}
			});

			editor.events.on('froalaEditor.drop', function(e, editor, dropEvent) {
				console.log(dropEvent);
			});
		}

		return {
			_init: _init,
			showCaptionPopup: showCaptionPopup,
			setCaption: setCaption
		};


		/**
		 * Refresh the CAPTION popup.
		 */

		function _refreshCaptionPopup () {
			var $current_image = this.image.get();

			if ($current_image) {
				var $popup = editor.popups.get('image.caption');
				$popup.find('textarea').val($current_image.attr('title') || '').trigger('change');
			}
		}

		/**
		 * Show the CAPTION popup.
		 */

		function showCaptionPopup () {
			var $popup = editor.popups.get('image.caption');
			if (!$popup) $popup = _initCaptionPopup();
			editor.popups.refresh('image.caption');
			editor.popups.setContainer('image.caption', $(editor.opts.scrollableContainer));

			var $current_image = editor.image.get();
			var left = $current_image.offset().left + $current_image.width() / 2;
			var top = $current_image.offset().top + $current_image.height();

			editor.popups.show('image.caption', left, top, $current_image.outerHeight());
		}

		/**
		 * Init the image upload popup.
		 */

		function _initCaptionPopup (delayed) {

			editor.popups.onRefresh('image.caption', _refreshCaptionPopup);

			var image_buttons = '';
			image_buttons = '<div class="fr-buttons">' + editor.button.buildList(editor.opts.imageCaptionButtons) + '</div>';

			var caption_layer = '';
			caption_layer = '<div class="fr-image-caption-layer fr-layer fr-active" id="fr-image-caption-layer-' + editor.id + '"><div class="fr-input-line"><textarea placeholder="' + editor.language.translate('Caption') + '" tabIndex="1" rows="3"></textarea></div><div class="fr-action-buttons"><button type="button" class="fr-command fr-submit" data-cmd="imageSetCaption" tabIndex="2">' + editor.language.translate('Update') + '</button></div></div>'

			var template = {
				buttons: image_buttons,
				caption_layer: caption_layer
			};

			var $popup = editor.popups.create('image.caption', template);

			if (editor.$wp) {
				editor.events.on('scroll.image-caption', function () {
					var $current_image = editor.image.get();

					if ($current_image && editor.popups.isVisible('image.caption')) {
						showCaptionPopup();
					}
				});
			}

			return $popup;
		}

		/**
		 * Set CAPTION based on the values from the popup.
		 */

		function setCaption (caption) {
			var $current_image = editor.image.get();

			if ($current_image) {
				var $popup = editor.popups.get('image.caption');
				$current_image.attr('title', caption || $popup.find('textarea').val() || '');
				$current_image.attr('alt', caption || $popup.find('textarea').val() || '');

				if (! caption) {
					caption = $popup.find('textarea').val()
				}

				var classes = $current_image.attr("class");
				var imageHtml = $current_image[0].outerHTML;

				var captionHtml = imageHtml;

				if (caption)
				{
					captionHtml = '<div class="post-caption-container fr-element" contenteditable="false"><figure class="thumbnail clearfix '+classes+'">'+imageHtml
						+'<figcaption class="caption pull-center fr-deletable">'+caption
						+'</figcaption></figure></div>';
				}

				if ($current_image.parent().parent().hasClass('post-caption-container'))
					$current_image.parent().parent().replaceWith(captionHtml);
				else
					$current_image.replaceWith(captionHtml);

				$popup.find('textarea:focus').blur();
				editor.popups.hide('image.caption');
			}
		}
	};

	$.FroalaEditor.DefineIcon('buttonIcon', { NAME: 'imageCaption'});

	$.FroalaEditor.RegisterCommand('imageCaption', {
		title: 'Caption',
		icon: '<i class="fa fa-tag"></i>',
		undo: true,
		focus: true,
		refreshAfterCallback: true,
		callback: function () {
			this.imageCaption.showCaptionPopup();
		}
	});

	$.FroalaEditor.RegisterCommand('imageSetCaption', {
		undo: true,
		focus: false,
		title: 'Update',
		refreshAfterCallback: false,
		callback: function () {
			this.imageCaption.setCaption();
		}
	});

})(jQuery);
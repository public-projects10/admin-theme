$(function()
{
	$.FroalaEditor.prototype.resetLinkValues = function () {
		this.$link_wrapper.find('input').val('');
		this.$link_wrapper.find('input[type="checkbox"].f-target').prop('checked', this.options.linkAlwaysBlank);
		this.$link_wrapper.find('input[type="text"].f-lt').val(this.text());
		this.$link_wrapper.find('input[type="text"].f-lu').val('');
		this.$link_wrapper.find('input[type="text"].f-lu').removeAttr('disabled');
		this.$link_wrapper.find('a.f-external-link, button.f-unlink').hide();

		for (var attr in this.options.linkAttributes) {
			this.$link_wrapper.find('input[type="text"].fl-' + attr).val('');
		}
	};

	var colors = [
		'#C1D72D', '#E90E8B', '#4aab46', '#242261', '#AF9244', '#ada4af', '#af733b',
		'#61BD6D', '#1ABC9C', '#54ACD2', '#2C82C9', '#9365B8', '#475577', '#CCCCCC',
		'#41A85F', '#00A885', '#3D8EB9', '#2969B0', '#553982', '#28324E', '#000000',
		'#F7DA64', '#FBA026', '#EB6B56', '#E25041', '#A38F84', '#EFEFEF', '#FFFFFF',
		'#FAC51C', '#F37934', '#D14841', '#B8312F', '#7C706B', '#D1D5D8', 'REMOVE'
	];
	var buttons = ['fullscreen', '|', 'undo', 'redo', '|', 'bold', 'italic', 'underline', '|', 'paragraphFormat', 'color', 'quote', 'align', '|', 'formatOL', 'formatUL',
		'-', 'insertLink', 'insertFile', 'insertImage', 'insertVideo', 'insertTweet', 'readMore', 'mediaLibrary', 'insertTable', 'clearFormatting', '|', 'html'];

	$('.wysiwyg-editor').froalaEditor({
		toolbarButtons: buttons,
		toolbarButtonsMD: buttons,
		toolbarButtonsSM: buttons,
		toolbarButtonsXS: buttons,
		charCounterCount: false,
		imageDefaultWidth: 0,
		imageUploadURL: '/api/media',
		imageEditButtons: ['imageReplace', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize', '-', 'imageCaption'],
		imageSplitHTML: true,
		fileUploadURL: '/api/media',
		heightMin: 200,
		saveInterval: 10000,
		linkAlwaysBlank: true,
		trackScroll: true,
		editorClass: 'word-count',
		linkAutoPrefix: '',
		linkAlwaysNoFollow: false,
		tableColors: colors,
		colorsText: colors,
		colorsBackground: colors,
		htmlDoNotWrapTags: ['script', 'style', 'img'],
		dragInline: false,
		lineBreakerTags: ['table', 'hr', 'iframe', 'form', 'dl', 'div'],
        htmlExecuteScripts: false

	});

	$('.wysiwyg-campaign-editor').froalaEditor({
		toolbarButtons: ['fullscreen', '|', 'undo', 'redo', '|', 'bold', 'italic', 'underline', '|', 'align', 'formatOL', 'formatUL',
			'|', 'insertLink', 'insertFile', 'insertImage', 'mediaLibrary', 'insertTable', 'clearFormatting', '|', 'html'],
		charCounterCount: false,
		imageDefaultWidth: 0,
		imageUploadURL: '/api/media',
		fileUploadURL: '/api/media',
		heightMin: 200,
		enter: $.FroalaEditor.ENTER_BR,
		linkAlwaysBlank: true,
		trackScroll: true,
		linkAutoPrefix: '',
		codeBeautifier: true,
		imageOutputSize: true,
        htmlExecuteScripts: false
	});

	$('.wysiwyg-editor-small').froalaEditor({
		toolbarButtons: ['bold', 'italic', 'underline', '|', 'formatOL', 'formatUL', '|', 'insertLink', 'insertFile', 'insertImage'],
		charCounterCount: false,
		heightMin: 150,
		imageUploadURL: '/api/media',
		fileUploadURL: '/api/media',
		spellcheck: true,
		linkAutoPrefix: '',
        htmlExecuteScripts: false
	});
});
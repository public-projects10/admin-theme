(function ($) {
	// Add an option for your plugin.
	$.FroalaEditor.DEFAULTS = $.extend($.FroalaEditor.DEFAULTS, {
		readMore: '<hr id="system-readmore">'
	});

	$.FroalaEditor.DefineIcon('buttonIcon', { NAME: 'readMore'});

	$.FroalaEditor.RegisterCommand('readMore', {
		// Button title.
		title: 'Insert read more line',

		// Specify the icon for the button.
		// If this option is not specified, the button name will be used.
		icon: '<i class="fa fa-arrow-circle-right"></i>',

		// Save the button action into undo stack.
		undo: true,

		// Focus inside the editor before the callback.
		focus: true,

		// Refresh the buttons state after the callback.
		refreshAfterCallback: true,

		// Called when the button is hit.
		callback: function () {
			// The current context is the editor instance.
			this.insertReadMore();
		}

	});

	$.FroalaEditor.prototype.insertReadMore = function (cmd) {
		// Insert HTML.
		var readMore = this.opts.readMore;
		var placeholder = '<div id="system-readmore-placeholder">&nbsp;</div>';

		this.html.insert(placeholder);

		var html = this.html.get();
		html = html.replace(readMore, '').replace(placeholder, readMore);
		this.html.set(html);

		// Save HTML in undo stack.
		this.undo.saveStep()
	}

})(jQuery);
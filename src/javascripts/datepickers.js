var dateFormat = 'j F Y';
var datetimeFormat = dateFormat + ' H:i';

Flatpickr.defaultConfig.altInput = true;
Flatpickr.defaultConfig.time_24hr = true;
Flatpickr.defaultConfig.defaultHour = 9;

$('.form-control-date').flatpickr({
    altFormat: dateFormat,
});

$('.form-control-datetime').flatpickr({
    altFormat: datetimeFormat,
    enableTime: true,
});

$('.form-control-daterange').flatpickr({
    altFormat: dateFormat,
    mode: "range",
});

$('.form-control-datetimerange').flatpickr({
    altFormat: datetimeFormat,
    enableTime: true,
    mode: 'range',
});
